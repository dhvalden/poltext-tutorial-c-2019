#!/usr/bin/env python3
import os
import json
import sys
import argparse

# Utility funtions

def validate_file(file_name):
    """
    validate file name and path.
    """
    MSG_INVALID_PATH = "Error: Invalid file path/name. Path %s does not exist."
    if not valid_path(file_name):
        print(MSG_INVALID_PATH%(file_name))
        quit()
    return


def valid_path(path):
    # validate file path
    return os.path.exists(path)


def spinning_cursor():
    while True:
        for cursor in "|/-\\":
            yield cursor

            
def _make_gen(reader):
    b = reader(1024 * 1024)
    while b:
        yield b
        b = reader(1024*1024)


def rawgencount(filename):
    f = open(filename, 'rb')
    f_gen = _make_gen(f.raw.read)
    return sum( buf.count(b'\n') for buf in f_gen )
            

def extract_text(tweet):
    full_text = "".encode()
    if "retweeted_status" in tweet:
        if "extended_tweet" in tweet["retweeted_status"]:
            full_text = tweet["retweeted_status"]["extended_tweet"]["full_text"]
        else:
            full_text = tweet["retweeted_status"]["text"]
    elif "quoted_status" in tweet:
        if "extended_tweet" in tweet["quoted_status"]:
            full_text = tweet["quoted_status"]["extended_tweet"]["full_text"]
        else:
            full_text = tweet["quoted_status"]["text"]
    elif "extended_tweet" in tweet:
        full_text = tweet["extended_tweet"]["full_text"]
    else:
        full_text = tweet["text"]
    return(full_text)


def ingester(line):
    tweet = json.loads(line)
    digest = {}
    digest["id"] = tweet["id_str"]
    digest["screen_name"] = tweet["user"]["screen_name"]
    digest["location"] = tweet["user"]["location"]
    digest["descripion"] = tweet["user"]["description"]
    digest["full_text"] = extract_text(tweet)

    return(digest)


def writer(args):

    input_file = args.input_file
    output_file = args.output_file

    validate_file(input_file)

    n_lines = rawgencount(input_file)
    sys.stdout.write("%s objects found. Processing... " % n_lines)

    counter = 0
    spinner = spinning_cursor()

    with open(input_file, "r", encoding="utf-8") as f,\
    open(output_file, "w", encoding="utf-8") as out:
        for line in f:
            try:
                output = ingester(line)
                out.write(json.dumps(output, ensure_ascii=False)+"\n")
            except:
                continue
            counter += 1
            if counter % 1000 == 0:
                sys.stdout.write(next(spinner))
                sys.stdout.flush()
                sys.stdout.write("\b")
                
    sys.stdout.write("Done!\n")


def main():
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument("input_file", type = str,
                        help = "Input file")

    parser.add_argument("output_file", type = str,
                        help = "Output file")

    # parse the arguments from standard input
    args = parser.parse_args()

    # calling functions depending on type of argument
    if args.input_file != None and args.output_file != None:
        writer(args)


if __name__ == '__main__':
	main()
