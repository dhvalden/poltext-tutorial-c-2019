import pandas as pd
import json
import re
import sys

import six
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types


data=[]
with open("digest.json", "r", encoding="utf-8") as f:
        for line in f:
            data.append(json.loads(line))
df = pd.DataFrame(data)

tweets = df["full_text"].tolist()


client = language.LanguageServiceClient()

# Detect and send native Python encoding to receive correct word offsets.
encoding = enums.EncodingType.UTF32
if sys.maxunicode == 65535:
    encoding = enums.EncodingType.UTF16

for tweet in tweets:

    if isinstance(tweet, six.binary_type):
            tweet = tweet.decode('utf-8')

    document = types.Document(
            content=tweet.encode('utf-8'),
            type=enums.Document.Type.PLAIN_TEXT)

    result = client.analyze_entity_sentiment(document, encoding)

    for entity in result.entities:
        print('Mentions: ')
        print(u'Name: "{}"'.format(entity.name))
        for mention in entity.mentions:
            print(u'  Begin Offset : {}'.format(mention.text.begin_offset))
            print(u'  Content : {}'.format(mention.text.content))
            print(u'  Magnitude : {}'.format(mention.sentiment.magnitude))
            print(u'  Sentiment : {}'.format(mention.sentiment.score))
            print(u'  Type : {}'.format(mention.type))
        print(u'Salience: {}'.format(entity.salience))
        print(u'Sentiment: {}\n'.format(entity.sentiment))

