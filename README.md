# Poltext Tutorial C 2019 Guide

Hey everyone! and welcome to the Tutorial C POLTEXT 2019 Guide.
This is meant to be a very simple, step by step guide, throughout the process of setting up your first virtual machine for data collection.
This guide will provide you with the commands and a brief explanation of the operation you are performing with each of them.
**This guide is only compatible with Bash and with Linux Ubuntu 18** although many commands are the same for other Linux distributions and shells.
Finally, this guide assumes you have administrator privileges over your system.

## Connecting to your virtual machine

During this tutorial you are going to work on a remote virtual machine. In order to connect to this remote machine you are going to use a secure shell, authenticated using a pair of keys.

First, open your terminal and run the following command:

```sh
ssh-keygen -t rsa
```

This command will create a pair of symmetric secret/public `ssh` keys in a directory called `/.shh` located  in your home directory. You must now go to this directory and open (using a text editor of your choice) the file ending with .pub, which correspond to the public key of the pair. 
Now copy the entire content of the file. Be careful not to add trailing white spaces. Open GCP Console, navigate to your VMs site, choose your VM, click on **Edit**, and in the section of **SSH keys** select **Add Item**. In the blank box that just appeared paste the copied public key.
In order to connect to connect to your VM, you will need its address, which in this case is its **External IP**. Find it, and copy it.
Finally, go back to your terminal and run the following command, replacing the sections `user_name` and `ip_address` with your information.

```sh
ssh -i id_rsa user_name@ip_address
```

A few warnings will appear. Accept all of them. After this you should be connected to your Ubuntu remote Virtual Machine.

## Installing packages and tools in our Virtual Machine

These commands will install the tools and packages we are going to use in our tutorial. This pacakes are being installed in the VM, not in your computer.

First, lets update the package database

```sh
sudo apt-get update
```

Now, lets install some packages. We are going to use the flag `-y` to globally accept the dependencies for our packages

First, lets install Git

```sh
sudo apt-get install -y git
```

Now, Python3 and pip

```sh
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
```

Now, lets install MongoDB. This requires some additional steps. Run these in order.

```sh
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
```
```sh
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
```
```sh
sudo apt-get update
```
```sh
sudo apt-get install -y mongodb-org
```

Once MongoDB is installed, we need to initialize it

```sh
sudo service mongod start
```

After this, you should be able to open the Mongo Shell by typing `mongo` and enter. You can quit the Mongo Shell by typing `exit` and enter.

Now, lets install some necesary python libraries

```sh
pip3 install tweepy pymongo pandas
```

This libraries will help us connect to the Twitter Streaming API using Python, and to our Mongo database to store our collected tweets.

Now, lets download this tutorial and its code to our virtual machine, so we can use it.
To achieve this, we are going to use the `git` command `clone` over this repository.

```sh
git clone https://gitlab.com/dhvalden/poltext-tutorial-c-2019.git
```
This command will download and create a directory in your home directory. Check it using the `ls` command.

Finally, we will move the content of this new directory to your home directory with the following command

```sh
mv ~/poltext-tutorial-c-2019/twt_collector ~/twt_collector
mv ~/poltext-tutorial-c-2019/tools/* ~/
```

## Configuring the credentials of our application

The `/twt_collector` directory you just downloaded contains the following files:

*  A "keys" file, which should store your Twitter credentials.
*  A "keywords" file, which contains the keywords you will use to filter the tweets you wish to collect.
*  A .service file, which will help us to create an independent service running in our machine.
*  And finally a python source file containing the code for our application.

For the sake of simplicity, the application we are going to use right now will look automatically in the keys and keywords files for the arguments it needs to run properly, but in future projects you may want to customize or even create your own application. I will also connect and dump every collected tweet to a mongo database called `twts`. The bits about configuring you database system are beyond the scope of this tutorial, mainly due to time constraints.

In order to start collecting tweets, we first need to setup our credentials. To do so, we just need to edit the file `keys` adding our Twitter credentials to each corresponding field.

```sh
CONSUMER_KEY=xxxxxxxxxxx
CONSUMER_SECRET=xxxxxxxxxxxxxxx
ACCESS_TOKEN=xxxxxxxxxxxxx
ACCESS_SECRET=xxxxxxxxxxxx
```

To edit it, use the following command which will open the file using the nano editor.

```sh
nano ~/twt_collector/keys
```

You can also add your own keywords to the `keywords` file using the same method.

```sh
nano ~/twt_collector/keywords
```
If you have done every properly, we should be ready to test our application, using the following command.

```sh
python3 ~/twt_collector/tweet_collector.py
```

## Automating the data collection 

You'll notice that by running the above command, your terminal will be locked. Also, if you close the connection, you application will be terminated. This is very inconvenient. To avoid that, we have several alternatives:

* We can use `crontab` to schedule a recurrent job. While this is good for short running applications, is not ideal in our case.
* We can use the `nohup` command, which ignores the **H**ang **Up** signal sent to the process after closing the connection. This can be useful, but it is difficult to control.
* Finally, we can set up a service, or "daemon", running in the background collecting data for us in a reliable way. **This is the method we are going to review**.

Use the following command to edit the "tweet_collector.service" file. 

```sh
nano ~/twt_collector/tweet_collector.service
```

You should see something like this.

```sh
[Unit]
Description=Collects tweets from the Twitter Streaming API
After=multi-user.target

[Service]
Type=simple
User=XXXXXX
ExecStart=/usr/bin/python3 /home/XXXXX/twt_collector/tweet_collector.py
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
```

Replace the "Xs" with your user name. You can find your current user name using the command `whoami`. Save the file and close it.

Now copy the file to the systemd directory, using the following command.

```sh
sudo cp ~/twt_collector/tweet_collector.service /etc/systemd/system/
```

This is a system directory, so you will need yo use `sudo`.

Finally, lets start our newly created service/daemon.

```sh
sudo systemctl start tweet_collector
```

And, lets enable automatic load and restart after reboot.

```sh
sudo systemctl enable tweet_collector
```

We can monitor and control our daemon using the `service` command
```sh
sudo service tweet_collector status # for status
sudo service tweet_collector start # to start it
sudo service tweet_collector stop # to stop it
sudo service tweet_collector restart # to restart it
```

## Managing our collected tweets

Rigth now, our data (tweets) are being streamed and store in a MongoDB collection. This provides security and redundancy, as well as some other utilities like search, spliting, and other management goodies. For now lets check how manmy tweets we have collected already.

Open mongo shell
```sh
mongo
```

List the available databases
```js
show dbs
```

Select your database
```js
use twts
```

List the available collections in your db
```js
show collections
```

Count how many documents have in your collection
```js
db.collection1.count()
```

Take a look at the documents being stored
```js
db.collection1.list()
```

Make a ramdom sample of 100 cases.

```js
db.collection1.aggregate( [ { $sample: { size: 100 } }, { $out: "sample"} ])
```

Finally, exit the mongo shell typing `exit`.

This is all well and good, but what if I want to use this data for analysis. We need to export it to a more portable format.

```sh
mongoexport --db twts --collection sample --out sample.json
```

Check for the file in your home directory with `ls`.

If you want to download this file to your local machine, open a new terminal and there issue the following command, replacing with your username and the IP of your virtual machine.
```sh
scp -i id_rsa user@IPaddress:~/sample.json ~/
```
This should download the sample.json file to your home directory in your local machine.

## Google Cloud NLP API

Now, we are going to briefly review how to perform some basic interactions with the GCP NLP API. This service is provided via an API. You cannot download the models, sadly, and the use of it comes with fees.

Before we start we are going to follow the quick startguide of GCP NLP API.

[Quickstart: Using Client Libraries](https://cloud.google.com/natural-language/docs/quickstart-client-libraries#client-libraries-install-python)

Now, before we can send our data for analysis in the NLP API, we must clean it a little. To do so, we will use a little script created for this purpose.

```sh
python3 ingester.py sample.json digest.json
```

Finally, we can now send our digested tweets to be anaylized by the Google NLP API.

```sh
python3 nlp_test.py
```

The response from the Google NLP API is a json object which needs to be parsed an processed, but for now we can just see the response in the screen.

## Bonus: Kubernetes Clusters

TBA.