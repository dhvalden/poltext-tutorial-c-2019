#!/usr/bin/env python3

import sys
import os
import argparse
import json
import tweepy
import datetime as date
from pymongo import MongoClient

def validate_file(file_name):
    if not os.path.exists(file_name):
        sys.exit("ERROR: The file %s does not exist" % file_name)


def read_keywords(file_name, save=False):
    with open(file_name, "r") as f:
        keywords = f.read().splitlines()
    if save:
        with open("keywords", "w") as o:
            for item in keywords:
                o.write("%s\n" % item)
    return keywords


def read_keys(file_name, key_names, save=False):
    credentials = {}
    with open(file_name, "r") as f:
        text = f.read()
        keylist = text.replace("=", " ").split()
    if save:
        with open("keys", "w") as o:
            o.write(text)
    for key in key_names:
        index = keylist.index(key)
        credentials[key] = keylist[index+1]
    return credentials


class StreamListener(tweepy.StreamListener):

    def __init__(self, api, mongo_host):
        super().__init__(api=api)
        self.mongo_host = mongo_host

    def on_connect(self):
        now = date.datetime.now()
        print("connection established at: " +
              now.strftime("%Y-%m-%d %H:%M:%S"))

    def on_error(self, status_code):
        now = date.datetime.now()
        print("an error has occured: " + repr(status_code) + "date: " +
              now.strftime("%Y-%m-%d %H:%M:%S"))
        return False

    def on_data(self, data):
        try:
            client = MongoClient(self.mongo_host)
            db = client.twts
            datajson = json.loads(data)
            db.collection1.insert_one(datajson)

        except Exception as e:
            now = date.datetime.now()
            print(e)
            print("date: " + now.strftime("%Y-%m-%d %H:%M:%S"))
            client.close()


def collect(args, default_keywords):

    keywords_file = args.keywords_file
    keys_file = args.keys_file
    mongo_host = args.mongo_host
    key_names = args.key_names

    validate_file(keywords_file)
    validate_file(keys_file)

    if keywords_file == default_keywords:
        keywords = read_keywords(keywords_file)
        credentials = read_keys(keys_file, key_names)
    else:
        keywords = read_keywords(keywords_file, save=True)
        credentials = read_keys(keys_file, key_names, save=True)
    try:
        auth = tweepy.OAuthHandler(credentials["CONSUMER_KEY"],
                                   credentials["CONSUMER_SECRET"])
        auth.set_access_token(credentials["ACCESS_TOKEN"],
                              credentials["ACCESS_SECRET"])
        # Set up the listener
        listener = StreamListener(api=tweepy.API(wait_on_rate_limit=True), mongo_host=mongo_host)
        streamer = tweepy.Stream(auth=auth, listener=listener)
        print("Tracking: " + str(keywords))
        streamer.filter(track=keywords)
    except Exception as e:
        now = date.datetime.now()
        print(e)
        print("date: " + now.strftime("%Y-%m-%d %H:%M:%S"))


def main():

    DEFAULT_MONGO_HOST = "mongodb://localhost/twts"
    DEFAULT_KEY_NAMES = ["CONSUMER_KEY", "CONSUMER_SECRET", "ACCESS_TOKEN",
                 "ACCESS_SECRET"]
    PATH = os.path.dirname(os.path.realpath(__file__))
    DEFAULT_KEYWORDS = PATH + "/keywords"
    DEFAULT_KEYS = PATH + "/keys"

    parser = argparse.ArgumentParser(description="tweet_collector")
    parser.add_argument("keywords_file", type=str, nargs="?",
                        metavar="keywords_file", default=DEFAULT_KEYWORDS)
    parser.add_argument("keys_file", type=str, nargs="?", metavar="keys_file",
                        default=DEFAULT_KEYS)
    parser.add_argument("mongo_host", type=str, nargs="?", metavar="mongo_host",
                        default=DEFAULT_MONGO_HOST)
    parser.add_argument("key_names", type=str, nargs="?", metavar="key_names",
                        default=DEFAULT_KEY_NAMES)
    args = parser.parse_args()
    if args.keywords_file is not None and args.keys_file is not None:
        collect(args, DEFAULT_KEYWORDS)


if __name__ == "__main__":
    main()
